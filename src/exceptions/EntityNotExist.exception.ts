import HttpException from "./Http.exception.js";

class EntityNotExistException extends HttpException {
    constructor(msg: string) {
        super(400, msg);
    }
}

export default EntityNotExistException;
