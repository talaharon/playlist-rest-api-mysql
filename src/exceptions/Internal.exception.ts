import HttpException from "./Http.exception.js";

class InternalException extends HttpException {
    constructor(msg: string) {
        super(500, msg);
    }
}

export default InternalException;
