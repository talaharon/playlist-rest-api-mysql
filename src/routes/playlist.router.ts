import express from "express";
import { verifyAuth } from "../middlewares/auth.middlewares.js";

const playlistRouter = express.Router();

playlistRouter.use(verifyAuth);

// Get all playlists
playlistRouter.get("/all");

// Get user's playlists
playlistRouter.get("/");

// Get playlist by ID
playlistRouter.get("/:id");

// Add a new playlist to my user
playlistRouter.post("/");

// Update a playlist
playlistRouter.put("/:id");

// Add song to playlist
playlistRouter.put("/playlist/:playlistId/song/:songId");

// Delete an playlist by ID
playlistRouter.delete("/:id");

// Delete song from playlist    
playlistRouter.delete("/playlist/:playlistId/song/:songId");

export default playlistRouter;
