import express from "express";
import AuthController from "../controllers/auth.controller.js";
import { verifyAuth } from "../middlewares/auth.middlewares.js";
import { bcryptPass, raw } from "../middlewares/common.middlewares.js";

const authRouter = express.Router();

authRouter.post("/register", bcryptPass, raw(AuthController.register));

authRouter.post("/login", raw(AuthController.login));

authRouter.post("/logout", verifyAuth, raw(AuthController.logout));

export default authRouter;
