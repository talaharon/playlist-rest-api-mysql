import express from "express";
import { verifyAuth } from "../middlewares/auth.middlewares.js";

const songRouter = express.Router();

songRouter.use(verifyAuth);

// Get approved songs
songRouter.get("/");

// Get all songs
songRouter.get("/all");

// Get pending songs
songRouter.get("/pending");

// Get song by id
songRouter.get("/:id");

// Add a song
songRouter.post("/");

// Update a song
songRouter.put("/:id");

// Approve/Reject a song
songRouter.put("/:id/status/:st");

// Delete a song
songRouter.delete("/:id");

export default songRouter;
