import express from "express";
import { verifyAuth } from "../middlewares/auth.middlewares.js";
import { bcryptPass } from "../middlewares/common.middlewares.js";

const userRouter = express.Router();

userRouter.use(verifyAuth);

// Get my user
userRouter.get("/");

// Get all users
userRouter.get("/all");

// Get user by ID
userRouter.get("/:id");

// Update a user
userRouter.put("/", bcryptPass);

// Delete my user
userRouter.delete("/:id");

// Delete a user by ID
userRouter.delete("/:id");


export default userRouter;
