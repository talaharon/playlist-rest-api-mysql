import express from "express";
import ArtistController from "../controllers/artist.controller.js";
import { raw } from "../middlewares/common.middlewares.js";
// import { verifyAuth } from "../middlewares/auth.middlewares.js";

// import log from '@ajar/marker';

const artistRouter = express.Router();

// artistRouter.use(verifyAuth);

// Get approved artists
artistRouter.get("/",raw(ArtistController.get_artists));

// Get all artists
artistRouter.get("/all", raw(ArtistController.get_all_artists));

// Get pending artists
artistRouter.get("/pending", raw(ArtistController.get_pending_artists));

// Get artist by id
artistRouter.get("/:id", raw(ArtistController.get_artist_by_id));

// Add an artist
artistRouter.post("/",raw(ArtistController.add_artist));

// Update an artist
artistRouter.put("/:id", raw(ArtistController.update_artist));

// Approve/Reject an artist
artistRouter.put("/:id/status/:st");

// Delete an Artist
artistRouter.delete("/:id");

export default artistRouter;
