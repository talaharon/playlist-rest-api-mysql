

export interface ISong {
    id: number,
    name: string,
    duration: number,
    genre: string,
    artist_id: number,
    status: "pending" | "rejected" | "approved",
    ref:number
}

