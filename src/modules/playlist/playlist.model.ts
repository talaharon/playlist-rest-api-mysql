

export interface IPlaylist {
    id: number,
    name: string,
    user_id: number,
}

