import ArtistAdapter from "./artist.adapter.js";
import { IArtist } from "./artist.model.js";


class ArtistService {
    async update_artist(artist_id: number, artist_payload: IArtist) {
        return await ArtistAdapter.update_artist(artist_id,artist_payload);
    }
    async add_artist(artist_payload: IArtist) {
        return await ArtistAdapter.add_artist(artist_payload,"pending");
    }
    async get_artist_by_id(artist_id : number) {
        return await ArtistAdapter.get_artist_by_id(artist_id);
    }
    async get_pending_artists() {
        return await ArtistAdapter.get_pending_artists();
    }

    async get_artists() {
        return await ArtistAdapter.get_approved_artists();
    }

    async get_all_artists() {
         return await ArtistAdapter.get_all_artists();
    }

}

export default new ArtistService();