
export interface IArtist {
    id: number,
    name: string,
    nickname: string,
    status: "pending" | "rejected" | "approved",
    ref: number
}

