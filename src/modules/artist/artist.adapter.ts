import { connection } from "../../db/sql.connection.js";
import { IArtist } from "./artist.model.js";
import {OkPacket} from 'mysql2';
import EntityNotExistException from "../../exceptions/EntityNotExist.exception.js";

class ArtistAdapter {
    async update_artist(artist_id: number, artist_payload: IArtist) {
        artist_payload.ref = artist_id;
        //await connection.query("UPDATE artist SET ? WHERE id=?",[artist_payload,artist_id]);
        const artist:IArtist = await this.add_artist(artist_payload,"pending");
        return artist;
    }

    async add_artist(artist_payload: IArtist,status:string) {
        const res = await connection.execute("INSERT INTO artist (name,nickname,status) VALUES (?,?,?)",[artist_payload.name,artist_payload.nickname,status]);
        artist_payload.id = (res as OkPacket[])[0].insertId;
        return artist_payload;
    }
    async get_artist_by_id(artist_id: number):Promise<IArtist> {
        const [artists] = await connection.execute("SELECT * FROM artist WHERE artist.id= ?",[artist_id]);
        if((artists as IArtist[]).length === 0){
            throw new EntityNotExistException(`Artist ${artist_id} does not exist`);
        }

        return (artists as IArtist[])[0];
    }

    async get_pending_artists() {
        const [rows] = await connection.execute("SELECT * FROM artist WHERE artist.status='pending'");
        return rows; 
    }

    async get_approved_artists() {
        const [rows] = await connection.execute("SELECT * FROM artist WHERE artist.status='approved'");
        return rows; 
    }

    async get_all_artists() {
        const [rows] = await connection.execute("SELECT * FROM artist");
        return rows;
    }

}

export default new ArtistAdapter();