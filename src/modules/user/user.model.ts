

export interface IUser {
    id: number,
    name: string,
    lastname: string,
    email: string,
    password: string,
    phone: string,
    roles:number,
    token: string
}

