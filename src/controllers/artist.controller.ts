import { Request, Response } from "express";
import ArtistService from "../modules/artist/artist.service.js";
class ArtistController {

    async get_pending_artists(req: Request, res: Response){
        const artists = await ArtistService.get_pending_artists();
        res.status(200).json(artists);
    }

    async get_all_artists(req: Request, res: Response) {
        const artists = await ArtistService.get_all_artists();
        res.status(200).json(artists);
    }

    async get_artists(req: Request, res: Response) {
        const artists = await ArtistService.get_artists();
        res.status(200).json(artists);
    }

    async get_artist_by_id(req: Request, res: Response) {
        const artists = await ArtistService.get_artist_by_id(Number(req.params.id));
        res.status(200).json(artists);
    }

    async add_artist(req: Request, res: Response) {
        const artist = await ArtistService.add_artist(req.body);
        res.status(200).json(artist);
    }

    async update_artist(req: Request, res: Response) {
        const artist = await ArtistService.update_artist(
            Number(req.params.id),
            req.body
        );
        res.status(200).json(artist);
    }

    // async deleteArtist(req: Request, res: Response) {
    //     const artist = await artist_service.delete_artist_by_id(req.params.id);
    //     if (!artist) {
    //         throw new DBException("Failed to delete artist");
    //     }
    //     res.status(200).json(artist);
    // }
}

export default new ArtistController();
