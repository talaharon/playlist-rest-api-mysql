import { Request, Response } from "express";
import DBException from "../exceptions/DB.exception.js";
import InternalException from "../exceptions/Internal.exception.js";
// import * as user_service from "../modules/user/user.service.js";
import jwt from "jsonwebtoken";
import bcrypt from "bcryptjs";
import ms from "ms";

class AuthController {
    async register(req: Request, res: Response) {
        const user = { refreshToken: "SS", id: 344 }; //await user_service.get_user_by_email(req.body.email);
        if (!user) {
            const user = { refreshToken: "SS", id: 344 }; //await user_service.create_user(req.body);
            if (!user) {
                throw new DBException("Failed to add user");
            }
            res.status(200).json(user);
        } else {
            throw new InternalException("User already exist");
        }
    }

    async login(req: Request, res: Response) {
        const {
            APP_SECRET,
            ACCESS_TOKEN_EXPIRATION,
            REFRESH_TOKEN_EXPIRATION,
        } = process.env;
        const { email, password } = req.body;
        const user = {
            _doc: { password: "SSS", s: "SSS" },
            refreshToken: "SS",
            _id: 344,
            email: "SSSS",
            password: "SSSS",
        }; //await user_service.get_user_by_email(email);
        if (
            user &&
            email === user.email &&
            (await bcrypt.compare(password, user.password))
        ) {
            // create a token
            const accessToken = jwt.sign(
                { _id: user._id },
                APP_SECRET as string,
                {
                    expiresIn: ms(ACCESS_TOKEN_EXPIRATION as string),
                }
            );
            const refreshToken = jwt.sign(
                { _id: user._id },
                APP_SECRET as string,
                {
                    expiresIn: ms(REFRESH_TOKEN_EXPIRATION as string),
                }
            );

            // await user_service.update_user_by_id(user._id, {
            //     refreshToken,
            // } as Partial<IUser>);

            const payload = { ...user._doc };

            // delete payload.password;

            res.cookie("refresh-token", refreshToken, {
                maxAge: ms(REFRESH_TOKEN_EXPIRATION as string),
                httpOnly: true,
            });
            res.cookie("access-token", accessToken, {
                maxAge: ms(ACCESS_TOKEN_EXPIRATION as string),
                httpOnly: true,
            });

            res.status(200).json({
                status: "Logged in",
                payload,
            });
        } else {
            res.status(403).json({
                status: "Unauthorized",
                payload: "Wrong email or password",
            });
        }
    }

    async logout(req: Request, res: Response) {
        const user = { refreshToken: "SS", id: 344 }; //await user_service.get_user_by_id(req.user_id);
        console.log(req.user_id);
        if (user) {
            // user.refreshToken = null;
            // user.save();
            res.clearCookie("access-token");
            res.clearCookie("refresh-token");
            res.status(200).json({
                status: "Logged out",
            });
        } else {
            res.status(403).json({
                status: "Unauthorized",
                payload: "Wrong email or password",
            });
        }
    }
}

export default new AuthController();
