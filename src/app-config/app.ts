import express from "express";
import morgan from "morgan";
import cors from "cors";
import common_router from "../routes/common.router.js";
import artist_router from "../routes/artist.router.js";
import user_router from "../routes/user.router.js";
import song_router from "../routes/song.router.js";
import playlist_router from "../routes/playlist.router.js";
import auth_router from "../routes/auth.router.js";
import cookieParser from "cookie-parser";

import {
    errorResponse,
    logErrorMiddleware,
    printError,
} from "../middlewares/errors.middlewares.js";
import { httpLogger } from "../middlewares/common.middlewares.js";
class App {
    public app: express.Application;

    constructor() {
        this.app = express();
        this.configGeneral();
        this.configMiddlewares();
        this.configRoutes();
        this.configErrorHandlers();
    }

    configErrorHandlers() {
        const { ERR_PATH = "./logs/PLACEHOLDER.errors.log" } = process.env;
        this.app.use(printError);
        this.app.use(logErrorMiddleware(ERR_PATH));
        this.app.use(errorResponse);
    }

    private configGeneral() {
        this.app.use(cors());
        this.app.use(cookieParser());
        this.app.use(morgan("dev"));
        this.app.use(express.json());
    }

    private configRoutes() {
        this.app.use("/api/auth", auth_router);
        this.app.use("/api/artist", artist_router);
        this.app.use("/api/user", user_router);
        this.app.use("/api/song", song_router);
        this.app.use("/api/playlist", playlist_router);
        this.app.use("*", common_router);
    }

    private configMiddlewares() {
        const { LOG_PATH = "./logs/PLACEHOLDER.http.log.txt" } = process.env;
        this.app.use(httpLogger(LOG_PATH));
    }
}

export default new App().app;
