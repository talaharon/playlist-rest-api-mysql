import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import ms from "ms";

export const verifyAuth = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const { APP_SECRET, ACCESS_TOKEN_EXPIRATION } = process.env;
    const { "refresh-token": refreshToken, "access-token": accessToken } =
        req.cookies;
    try {
        // verifies secret and checks exp
        const decoded = await jwt.verify(accessToken, APP_SECRET as string);

        // if everything is good, save to request for use in other routes
        req.user_id = decoded._id;
        next();
    } catch (error) {
        try {
            const decoded = await jwt.verify(
                refreshToken,
                APP_SECRET as string
            );
            const user = { refreshToken: "SS", id: 344 }; //await user_service.get_user_by_id(decoded._id);
            if (user.refreshToken !== refreshToken) {
                throw new Error("");
            }
            req.user_id = decoded._id;
            // create a token
            const accessToken = jwt.sign(
                { _id: user.id },
                APP_SECRET as string,
                {
                    expiresIn: ms(ACCESS_TOKEN_EXPIRATION as string),
                }
            );
            res.cookie("access-token", accessToken, {
                maxAge: ms(ACCESS_TOKEN_EXPIRATION as string),
                httpOnly: true,
            });
            next();
        } catch {
            return res.status(401).json({
                status: "Unauthorized",
                payload: "Unauthorized - Failed to authenticate token.",
            });
        }
    }
};
