import log from "@ajar/marker";
import {
    ErrorRequestHandler,
    NextFunction,
    RequestHandler,
    Request,
    Response,
} from "express";
import HttpException from "../exceptions/Http.exception.js";
import UrlNotFoundException from "../exceptions/UrlNotFound.exception.js";
import fs from "fs";

const { NODE_ENV } = process.env;

export const printError: ErrorRequestHandler = (err, req, res, next) => {
    log.error(err);
    next(err);
};

export function logErrorMiddleware(path: string): ErrorRequestHandler {
    // https://nodejs.org/api/fs.html#file-system-flags
    // 'a': Open file for appending. The file is created if it does not exist.
    const errorsFileLogger = fs.createWriteStream(path, { flags: "a" });

    return (
        error: HttpException,
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        // new log entry ->

        errorsFileLogger.write(
            `${error.status} :: ${error.message} >> ${error.stack} \n`
        );
        // failureResponse(error, res);
        next(error);
    };
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const errorResponse: ErrorRequestHandler = (err, req, res, next) => {
    if (NODE_ENV !== "production")
        res.status(err.status).json({
            status: err.status,
            message: err.message,
            stack: err.stack,
        });
    else
        res.status(err.status).json({
            status: err.status,
            message: err.message,
        });
};

export const not_found: RequestHandler = (req, res, next) => {
    next(new UrlNotFoundException(req.baseUrl));
};
