// require('dotenv').config();
import log from '@ajar/marker';
import app from './app-config/app.js';



const { PORT = 8080,HOST = 'localhost' } = process.env;

//start the express api server
;(async ()=> {
  //connect to mongo db
  await app.listen(Number(PORT),HOST);
  log.magenta(`api is live on`,` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);  
})().catch(console.log);